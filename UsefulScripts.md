### Useful scripts
 * To update database:
    * To activate environment
        ` conda activate igwn-py37`
    * To update catalog (GWOSC data only):
    
        `python ./updatecat.py --skipgracedb -u --devMode -v`
    * To update entire catalog and force download (GWOSC data only):
    
        `python ./updatecat.py --skipgracedb -u -f -5 -m --devMode -v`
    * To generate maps, gravoscope and waveforms (but no data update):
    
        `python ./updatecat.py --skipgracedb -s -g -w --devMode -v`
 * To copy data files (not part of repo):
    * FITS files (maps):
    
        `rsync -avP --include='*.fits*' --include='*/' --exclude='*' chris.north@ligo.gravity.cf.ac.uk:public_html/LVC/gwcat-data-dev/data/fits/ ./data/fits/`
    * h5 files (data):
    
        `rsync -avP --include='*.h*' --include='*/' --exclude='*' chris.north@ligo.gravity.cf.ac.uk:public_html/LVC/gwcat-data-dev/data/h5/ ./data/h5/`
 * To update codebase:
    * Update submodules:
    
        `git submodule update --remote`